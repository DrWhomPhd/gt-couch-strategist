use bytes::{Bytes, BytesMut};
use clap::{Parser, Subcommand};
use gtstats_rs::Lap;
use gtstats_rs::Race;
use slint::ModelRc;
use slint::SharedString;
use slint::StandardListViewItem;
use slint::VecModel;
use slint::Weak;
use std::borrow::BorrowMut;
use std::char::REPLACEMENT_CHARACTER;
use std::rc::Rc;
use std::sync::RwLock;
use std::thread::current;
use std::thread::Thread;
use std::{env, sync::Arc};
use tokio::io::{self, AsyncReadExt, AsyncWriteExt};
use tokio::net::TcpStream;

// Clap Derive Command Line Args
#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct CliArgs {
    /// Sets server ip/name
    #[arg(short, long, value_name = "SERVER")]
    server: String,

    /// Sets port
    #[arg(short, long, value_name = "PORT")]
    port: String,

    /// Turn debugging information on
    #[arg(short, long, action = clap::ArgAction::Count)]
    debug: u8,
}

// Allows for the safe passing of race data between threads
struct ThreadSafeRace {
    race: RwLock<Race>,
}
impl ThreadSafeRace {
    pub fn new(race: Race) -> Arc<ThreadSafeRace> {
        Arc::new(ThreadSafeRace {
            race: RwLock::new(race),
        })
    }
}

/// Returns: Position in buffer after the packet end.
pub fn find_packet_end(buf: &BytesMut) -> Option<usize> {
    // Determine if we found the first byte of the \r\n pair
    for b in 0..buf.len() {
        if buf[b] == b'\r' {
            if b + 1 < buf.len() {
                if buf[b + 1] == b'\n' {
                    return Some(b + 2);
                }
            }
        }
    }
    None
}

async fn process(packet: BytesMut, race: Arc<ThreadSafeRace>) {
    let stringified_packet = String::from_utf8_lossy(&packet)
        .trim_end()
        .replace(REPLACEMENT_CHARACTER, "¦");
    gtstats_rs::multiloop::parse_multiloop_line(
        &mut race.race.write().unwrap(),
        &stringified_packet,
    );
    println!("PACKET {}", stringified_packet);
}

async fn network_thread(
    race: Arc<ThreadSafeRace>,
    main_window_weak: Weak<MainWindow>,
    server: &str,
    port: &str,
) {
    let connection = TcpStream::connect(format!("{}:{}", server, port))
        .await
        .unwrap();
    let (mut rd, _) = io::split(connection);

    let mut buf = BytesMut::with_capacity(2048);

    loop {
        let main_window_weak = main_window_weak.clone();
        let race_for_process = race.clone();
        rd.read_buf(&mut buf).await.unwrap();
        if let Some(cnt) = find_packet_end(&buf) {
            let rest = buf.split_off(cnt);
            let race_for_ui = race_for_process.clone();
            // Send packet off for parsing with the greater stats engine
            let _ = tokio::spawn(async move { process(buf, race_for_process).await });

            buf = rest;

            // spawn a UI update
            let _ = tokio::spawn(async move {
                update_ui(main_window_weak, race_for_ui).await;
            });
        };
    }
}

async fn update_ui(handle: Weak<MainWindow>, race: Arc<ThreadSafeRace>) {
    let _ = handle.upgrade_in_event_loop(move |handle| {
        handle.set_race_name(SharedString::from(race.race.read().unwrap().name.clone()));
        let mut car_stats: Vec<ModelRc<StandardListViewItem>> = Vec::default();
        for car in race.race.read().unwrap().cars.values() {
            let current_lap = car.laps.keys().max().unwrap_or(&0);
            let (last_lap, int_to_next, int_to_lead) = {
                if let Some(lap) = car.laps.get(current_lap) {
                    (*current_lap, lap.inter_to_next, lap.inter_to_leader)
                } else {
                    (0, 0, 0)
                }
            };
            car_stats.push(VecModel::from_slice(&[
                StandardListViewItem::from(car.number.as_str()),
                StandardListViewItem::from(format!("{}", current_lap).as_str()),
                StandardListViewItem::from(format!("{}", last_lap).as_str()),
                StandardListViewItem::from(format!("{}", int_to_next).as_str()),
                StandardListViewItem::from(format!("{}", int_to_lead).as_str()),
            ]))
        }
        handle.set_stat_rows(VecModel::from_slice(car_stats.as_slice()));
    });
}

#[tokio::main]
async fn main() {
    let mut race = Race::empty();

    let main_window = MainWindow::new().unwrap();

    let args = CliArgs::parse();

    let mut race_weak = ThreadSafeRace::new(race);
    let main_window_weak = main_window.as_weak();

    let race_for_network = race_weak.clone();
    let _ = tokio::spawn(async move {
        network_thread(race_for_network, main_window_weak, &args.server, &args.port).await
    });

    let race_for_ui = race_weak.clone();

    main_window.run().unwrap();
}

slint::slint! {
    import { Button, VerticalBox , TabImpl, StandardTableView} from "std-widgets.slint";
    export component MainWindow inherits Window {
        in-out property <string> race_name: "GT Couch Strategist";
        in-out property <[[StandardListViewItem]]> stat_rows:
                [[{text: "" },
                {text: "" },
                {text: "" },
                {text: "" },
                {text: "" }]];
        GridLayout {
            Row {
                Text {
                    text: root.race_name;
                    font-size: 24px;
                    horizontal-alignment: center;
                }
            }
            Row {
                StandardTableView {
                    columns: [
                        {title: "Car #" },
                        {title: "Lap #" },
                        {title: "Last Lap Time"},
                        {title: "Int to next"},
                        {title: "Int to leader"},
                    ];
                    rows: root.stat-rows;

                }
            }
        }
    }
}
